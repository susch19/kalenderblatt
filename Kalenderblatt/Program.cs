﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalenderblatt
{
    class Program
    {
        static void Main(string[] args)
        {
#region Initialisierung
            var cki = new ConsoleKeyInfo();

            int year = 2016; //is used to output monday to sunday in the console as a loop
            int month = 8;
            var yearsuccess = false;
            int yearUser = 0;
            int monthUser = 0;
#endregion
            while (cki.KeyChar != 'b') //exit condition
            {
#region UserEingabe
                Console.Clear();
                if (!yearsuccess) //user inputs a year and if that fails, the user can decide, if he wants to close the program or retry
                {
                    Console.Write("Geben sie ein Jahr ein: ");
                    if (!int.TryParse(Console.ReadLine(), out yearUser))
                    {
                        Console.Clear();
                        Console.WriteLine("Sie haben ein falsches Jahr eingegeben!");
                        Console.WriteLine("Zum Beenden b drücken oder zum Wiederholen eine beliebige Taste");
                        cki = Console.ReadKey();
                        continue;
                    }

                }
                yearsuccess = true;
                Console.Write("Geben sie ein Monat in Zahlen ein: "); //user inputs a month and if that fails, the user can decide, if he wants to close the program or retry to input month or year
                if (!int.TryParse(Console.ReadLine(), out monthUser) || monthUser < 0 || monthUser > 12)
                {
                    Console.Clear();
                    Console.WriteLine("Sie haben den Monat falsch eingegeben! Bitte nur Zahlen zwischen 1 und 12.");
                    Console.WriteLine("Zum Beenden b drücken \r\nUm Das Jahr noch mal einzugeben j drücken \r\nUm nur den Monat noch mal einzugeben beliebige Taste drücken");
                    cki = Console.ReadKey();
                    yearsuccess = cki.KeyChar == 'j' ? false : true;
                    continue;
                }
                yearsuccess = false;

                Console.Clear();
                #endregion
#region Kalenderaufbau
                var td = new TimeDate(1, month, year);

                for (td.Day = 1; td.Day < 8; td.Day++) //Monday to Sunday in console (first 2 letters)
                    Console.Write(td.GetNameOfDay().Substring(0,2).PadRight(6,' '));

                td = new TimeDate(1, monthUser, yearUser );

                Console.WriteLine();
                Console.Write("".PadRight(6 * td.DeterminationOfTheDayOfTheWeek(0), ' '));

                for (td.Day = 1; td.Day <= td.DaysInMonth; td.Day++) //printing of the lines per month
                {
                    Console.Write(td.Day.ToString().PadRight(6, ' '));
                    if (td.GetNameOfDay() == "Sonntag")
                        Console.WriteLine();
                }
                #endregion

                Console.WriteLine();
                Console.Write("Drücken sie b zum Beenden oder eine andere Taste zum fortfahren"); //exit if user presses b, otherwise start all over again
                cki = Console.ReadKey();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalenderblatt
{
    class TimeDate
    {
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public int DaysInMonth { get { return DaysInThisMonth(); } }

        private int day;
        private int month;
        private int year;

        public TimeDate(int day, int month, int year)
        {
            Day = day;
            Month = month;
            Year = year;
        }
        /// <summary>
        /// uses the current Day property to return the name of the day
        /// </summary>
        /// <returns>name of the day</returns>
        public string GetNameOfDay()
        {
            switch (DeterminationOfTheDayOfTheWeek(Day))
            {
                case 0: return "Sonntag";
                case 1: return "Montag";
                case 2: return "Dienstag";
                case 3: return "Mittwoch";
                case 4: return "Donnerstag";
                case 5: return "Freitag";
                case 6: return "Samstag";
                default: return "";
            }
        }

        public string GetNameOfMonth()
        {
            var li = new List<string> { "Januar", "Februar", "März", "April", "Mai", "Juni", "Juli",
                                        "August", "September", "Oktober", "November", "Dezember"};
            return li[Month - 1];
        }

        private int DeterminationOfTheFirstDayOfAMonth() => DeterminationOfTheDayOfTheWeek(1);
        
        /// <summary>
        /// Calculates the day of the week with the Georg Glaeser algorithm
        /// </summary>
        /// <param name="day">the day of the month</param>
        /// <returns>the day of the week as an integer, starting at 0 for sunday</returns>
        public int DeterminationOfTheDayOfTheWeek(int day)
        {
            int tempyear = Month < 3 ? Year - 1 : Year;
            //int lastTwoDigits = int.Parse((Month < 3 ? Year - 1 : Year).ToString().Substring(2));
            int lastTwoDigits = tempyear - tempyear / 100 * 100;
            //int firstTwoDigits = int.Parse((Month < 3 ? Year - 1 : Year).ToString().Substring(0, 2));
            int firstTwoDigits = tempyear / 100;
            var d = (int)(day + Math.Truncate(2.6 * (((Month + 9) % 12) + 1) - 0.2) +
                lastTwoDigits +
                Math.Truncate(lastTwoDigits / 4d) +
                Math.Truncate(firstTwoDigits / 4d) - 2 * firstTwoDigits) % 7;
            while (d < 0)
                d += 7;
            return d;
        }

        /// <summary>
        /// Determines the amount of days of the current month
        /// </summary>
        /// <returns>amount of days</returns>
        private int DaysInThisMonth()
        {
            switch (Month)
            {
                case 2: return Year % 4 != 0 ? 28 : Year % 100 == 0 ? Year % 400 == 0 ? 29 : 28 : 29;
                case 1: case 3: case 5: case 7: case 8: case 10: case 12: return 31;
                default: return 30;
            }
        }

    }
}
